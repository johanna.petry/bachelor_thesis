#ifndef DF_NAIVE_H
#define DF_NAIVE_H

#include "compute_distances.h"

namespace SDFNaive {

using namespace Eigen;

void sdf_naive(ComputeDistances::DistanceTree* distance_tree, MatrixXd dimensions, VectorXd* distance_field);

}

#endif // DF_NAIVE_H
