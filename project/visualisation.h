#ifndef VISUALISATION_H
#define VISUALISATION_H

#include <Eigen/Eigen>

namespace Visualization {

using namespace Eigen;

void plot(MatrixXd* V, MatrixXi* F, VectorXd* distance_field, MatrixXd dimensions, bool show_original_mesh);

}

#endif // VISUALISATION_H
