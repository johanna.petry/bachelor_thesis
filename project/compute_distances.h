#ifndef COMPUTE_DISTANCES_H
#define COMPUTE_DISTANCES_H

#include <Eigen/Eigen>
#include <igl/AABB.h>
#include <igl/fast_winding_number.h>

namespace ComputeDistances {

using namespace Eigen;

class DistanceTree {
public:
    Eigen::MatrixXd V;
    Eigen::MatrixXi F;
    igl::AABB<Eigen::MatrixXd, 3> aabb_tree;
    igl::FastWindingNumberBVH winding_number_bvh;

    DistanceTree() {}
};

void get_distance_tree(MatrixXd* V, MatrixXi* F, DistanceTree* distance_tree);
void distance_to_object(MatrixXd* indices, DistanceTree* distance_tree, VectorXd* distances);

}

#endif // COMPUTE_DISTANCES_H
