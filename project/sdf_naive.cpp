#include "sdf_naive.h"

namespace SDFNaive {

using namespace Eigen;

void create_voxelgrid(MatrixXd dimensions,VectorXd size, MatrixXd* indices) {
    int index_x = 0;
    int index_y = 0;
    int index_z = 0;
    for (int z = dimensions(1, 2); z <= dimensions(0, 2); z++) {
        index_y = 0;
        for (int y = dimensions(1, 1); y <= dimensions(0, 1); y++) {
            index_x = 0;
            for (int x = dimensions(1, 0); x <= dimensions(0, 0); x++) {

                int index = index_z * (size(0) * size(1)) + index_y * size(0) + index_x;
                (*indices)(index, 0) = x;
                (*indices)(index, 1) = y;
                (*indices)(index, 2) = z;

                index_x++;
            }
            index_y++;
        }
        index_z++;
    }
}

void sdf_naive(ComputeDistances::DistanceTree* distance_tree, MatrixXd dimensions, VectorXd* distance_field) {

    std::chrono::steady_clock::time_point begin = std::chrono::steady_clock::now();

    // create voxelgrid (get all indices where we want to compute a distance)
    VectorXd size = dimensions.row(0) - dimensions.row(1) + RowVector3d(1, 1, 1);
    int num_complete_indices = size(0) * size(1) * size(2);
    std::unique_ptr<MatrixXd> indices = std::unique_ptr<MatrixXd>(new MatrixXd(num_complete_indices, 3));
    create_voxelgrid(dimensions, size, indices.get());

    // get distance field
    ComputeDistances::distance_to_object(indices.get(), distance_tree, distance_field);

    std::chrono::steady_clock::time_point end = std::chrono::steady_clock::now();
    int timing = std::chrono::duration_cast<std::chrono::milliseconds>(end - begin).count();

    //output
    std::cout << "\n" << "distance field naive:" << std::endl;
    std::cout << timing << "ms" << std::endl;
}

}
