#include "compute_distances.h"
#include <igl/signed_distance.h>

using namespace Eigen;

namespace ComputeDistances {

void get_distance_tree(MatrixXd* V, MatrixXi* F, DistanceTree* distance_tree) {
    MatrixXd FN, VN, EN;
    VectorXi EMAP;
    MatrixXi E;

    igl::AABB<MatrixXd, 3> tree;
    tree.init((*V), (*F));
    igl::per_face_normals((*V), (*F), FN);
    igl::per_vertex_normals((*V), (*F), igl::PER_VERTEX_NORMALS_WEIGHTING_TYPE_ANGLE, FN, VN);
    igl::per_edge_normals((*V), (*F), igl::PER_EDGE_NORMALS_WEIGHTING_TYPE_UNIFORM, FN, EN, E, EMAP);

    distance_tree->V = (*V);
    distance_tree->F = (*F);
    distance_tree->aabb_tree = tree;

    igl::fast_winding_number((*V), (*F), 2, distance_tree->winding_number_bvh);

}

void distance_to_object(MatrixXd* indices, DistanceTree* distance_tree, VectorXd* distances) {

    igl::signed_distance_fast_winding_number(*indices, distance_tree->V, distance_tree->F, distance_tree->aabb_tree, distance_tree->winding_number_bvh, *distances);
}

}
