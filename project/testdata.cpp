#include "testdata.h"
#include <fstream>
#include <stdexcept>
#include <igl/readOFF.h>

namespace TestData {

using namespace Eigen;

void resize_and_get_dimenson(MatrixXd* V, float resize_factor, MatrixXd* dimensions) {
    // rezise object
    (*V) *= resize_factor;

    // get boundaries values
    VectorXd max_val = V->colwise().maxCoeff();
    VectorXd min_val = V->colwise().minCoeff();
    for (int i = 0; i < 3; i++) {
        (*dimensions)(0, i) = ceil(max_val(i));
        (*dimensions)(1, i) = floor(min_val(i));
    }
}

void translate_center(MatrixXd* V, MatrixXd* dimensions_object, int cube_length) {

    // get center of object
    VectorXd center_obj(3);
    for (int i = 0; i < 3; i++) {
        int max = (*dimensions_object)(0, i);
        int min = (*dimensions_object)(1, i);
        if (max >= 0 && min >= 0) {
            center_obj(i) = min + (max - min) / 2;
        }
        else if (max < 0 && min < 0) {
            center_obj(i) = min + (abs(min) - abs(max)) / 2;
        }
        else {
            center_obj(i) = min + (max + abs(min)) / 2;
        }
    }



    // translate object in center of cube
    VectorXd translate(3);
    int center_cube = cube_length / 2;
    for (int i = 0; i < 3; i++) {
        translate(i) = (center_obj(i) >= 0) ? (center_cube - center_obj(i)) : (center_cube + abs(center_obj(i)));
    }
    (*V) = (*V).rowwise() + translate.transpose();

}

void rotate(MatrixXd* V, MatrixXd* dimensions_object, input_object object) {
    MatrixXd rotate_matrix(3, 3);
    std::vector<input_object> bunnies = {input_object::bunny_1k, input_object::bunny_1mio, input_object::bunny_4mio};
    if(std::find(bunnies.begin(), bunnies.end(), object) != bunnies.end()) {
        rotate_matrix << 1,0,0,
                0,0,1,
                0,-1,0;
    }
    else if (object == input_object::armadillo) {
        rotate_matrix << -1, 0, 0,
                0, 1, 0,
                0, 0, -1;
    }
    else {
        return;
    }

    for (int i = 0; i < V->rows(); i++) {
        Vector3d col_vec = (*V).row(i).transpose();
        col_vec = rotate_matrix * col_vec;
        (*V).row(i) = col_vec.transpose();
    }

    for (int i = 0; i < dimensions_object->rows(); i++) {
        Vector3d col_vec = (*dimensions_object).row(i).transpose();
        col_vec = rotate_matrix * col_vec;
        (*dimensions_object).row(i) = col_vec.transpose();
    }
}

void get_object(float resize_factor, input_object object, int cube_length, MatrixXd* V, MatrixXi* F, MatrixXd* dimensions) {

    switch(object) {
        case input_object::bunny_1k:
            igl::readOFF("../project/testdata/bunny_1k.off", (*V), (*F));
            break;
        case input_object::bunny_1mio:
            igl::readOFF("../project/testdata/bunny_1mio.off", (*V), (*F));
            break;
        case input_object::bunny_4mio:
            igl::readOFF("../project/testdata/bunny_4mio.off", (*V), (*F));
            break;
        case input_object::puzzle_g1:
            igl::readOFF("../project/testdata/g1.off", (*V), (*F));
            break;
        case input_object::dragon_900k:
            igl::readOFF("../project/testdata/dragon_900k.off", (*V), (*F));
            break;
        case input_object::dragon_7mio:
            igl::readOFF("../project/testdata/dragon_7mio.off", (*V), (*F));
            break;
        case input_object::armadillo:
            igl::readOFF("../project/testdata/armadillo.off", (*V), (*F));
            break;
        case input_object::buddha:
            igl::readOFF("../project/testdata/buddha.off", (*V), (*F));
            break;
    }

    if (V->size() <= 0 || F->size() <= 0) {
        throw std::invalid_argument("object does not contain any points or triangles");
    }

    rotate(V, dimensions, object);

    if(object == input_object::dragon_900k) {
        resize_and_get_dimenson(V, 1000 * resize_factor, dimensions);
    }
    else if (object == input_object::puzzle_g1) {
        resize_and_get_dimenson(V, 24 * resize_factor, dimensions);
    }
    else if (object == input_object::buddha) {
        resize_and_get_dimenson(V, 1200 * resize_factor, dimensions);
    }
    else {
        resize_and_get_dimenson(V, resize_factor, dimensions);
    }


    // translate to center of distance field
    translate_center(V, dimensions, cube_length);

}

}
