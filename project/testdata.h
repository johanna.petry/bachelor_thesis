#ifndef TESTDATA_H
#define TESTDATA_H

#include <Eigen/Eigen>

namespace TestData{

using namespace Eigen;

// available testmeshes
enum class input_object {bunny_1k, bunny_1mio, bunny_4mio, puzzle_g1, dragon_900k, dragon_7mio, armadillo, buddha};

void get_object(float resize_factor, input_object object, int cube_length, MatrixXd* V, MatrixXi* F, MatrixXd* dimensions);

}

#endif // TESTDATA_H
