#include "visualisation.h"
#include <igl/opengl/glfw/Viewer.h>
#include <igl/marching_cubes.h>

namespace Visualization {

using namespace Eigen;

void create_voxelgrid(MatrixXd dimensions,VectorXd size, MatrixXd* indices) {
    int index_x = 0;
    int index_y = 0;
    int index_z = 0;
    for (int z = dimensions(1, 2); z <= dimensions(0, 2); z++) {
        index_y = 0;
        for (int y = dimensions(1, 1); y <= dimensions(0, 1); y++) {
            index_x = 0;
            for (int x = dimensions(1, 0); x <= dimensions(0, 0); x++) {

                int index = index_z * (size(0) * size(1)) + index_y * size(0) + index_x;
                (*indices)(index, 0) = x;
                (*indices)(index, 1) = y;
                (*indices)(index, 2) = z;

                index_x++;
            }
            index_y++;
        }
        index_z++;
    }
}

igl::opengl::glfw::Viewer updateView(MatrixXd V, MatrixXi F, igl::opengl::glfw::Viewer viewer, VectorXd size) {

    // mesh
    viewer.data().set_mesh(V, F);

    // colors
    viewer.core().background_color.setOnes();
    MatrixXd mesh_color(F.rows(), 3);
    for (int i = 0; i < F.rows(); i++) {
        mesh_color.row(i) = RowVector3d(0.8, 0, 0.16);
    }
    viewer.data().set_colors(mesh_color);
    viewer.data().set_face_based(true);

    // box
    MatrixXd box(2, 3);
    box << 0, 0, 0, size(0), size(1), size(2);

    MatrixXd v_box(8,3);
    v_box <<
            box(0,0), box(0,1), box(0,2),
            box(1,0), box(0,1), box(0,2),
            box(1,0), box(1,1), box(0,2),
            box(0,0), box(1,1), box(0,2),
            box(0,0), box(0,1), box(1,2),
            box(1,0), box(0,1), box(1,2),
            box(1,0), box(1,1), box(1,2),
            box(0,0), box(1,1), box(1,2);

    MatrixXi e_box(12, 2);
    e_box <<
             0, 1,
            1, 2,
            2, 3,
            3, 0,
            0, 4,
            1, 5,
            2, 6,
            3, 7,
            4, 5,
            5, 6,
            6, 7,
            7, 4;

    for (unsigned i = 0; i < e_box.rows(); i++) {
        viewer.data().add_edges(
                    v_box.row(e_box(i,0)),
                    v_box.row(e_box(i,1)),
                    RowVector3d(0,0,0));
    }

    return viewer;
}

void plot(MatrixXd* V, MatrixXi* F, VectorXd* distance_field, MatrixXd dimensions, bool show_original_mesh) {

    std::cout << "\n" << std::endl;

    VectorXd size = dimensions.row(0) - dimensions.row(1) + RowVector3d(1, 1, 1);
    int num_complete_indices = size(0) * size(1) * size(2);
    std::unique_ptr<MatrixXd> indices = std::unique_ptr<MatrixXd>(new MatrixXd(num_complete_indices, 3));
    create_voxelgrid(dimensions, size, indices.get());

    MatrixXd SV;
    MatrixXi SF;
    int isolevel = 0;

    igl::opengl::glfw::Viewer viewer;
    if (show_original_mesh) {
        viewer = updateView((*V), (*F), viewer, size);
    }
    else {
        igl::marching_cubes((*distance_field), (*indices), size(0), size(1), size(2), isolevel, SV, SF);
        viewer = updateView(SV, SF, viewer, size);
    }

    // marching cubes
    viewer.callback_key_down = [&](igl::opengl::glfw::Viewer & viewer, unsigned char key, int mod)->bool {
        switch(key) {
            default:
                return false;
            case '0':
                isolevel = 0;
                igl::marching_cubes((*distance_field), (*indices), size(0), size(1), size(2), isolevel, SV, SF);
                viewer.data().clear();
                viewer = updateView(SV, SF, viewer, size);
                break;
            case '1':
                isolevel++;
                igl::marching_cubes((*distance_field), (*indices), size(0), size(1), size(2), isolevel, SV, SF);
                viewer.data().clear();
                viewer = updateView(SV, SF, viewer, size);
                break;
            case '2':
                isolevel--;
                igl::marching_cubes((*distance_field), (*indices), size(0), size(1), size(2), isolevel, SV, SF);
                viewer.data().clear();
                viewer = updateView(SV, SF, viewer, size);
                break;
            case '3':
                if (show_original_mesh) {
                    isolevel = 0;
                    viewer.data().clear();
                    viewer = updateView((*V), (*F), viewer, size);
                }
                break;
        }
        viewer.data().set_face_based(true);
        return true;
    };
    viewer.launch();
}

}
