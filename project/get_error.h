#ifndef GET_ERROR_H
#define GET_ERROR_H

#include "Eigen/Eigen"

namespace GetError {
    using namespace Eigen;

    void get_error(VectorXd* sdf_naive, VectorXd* sdf_accelerated, double* mean_error, double* max_error);
}


#endif // GET_ERROR_H
