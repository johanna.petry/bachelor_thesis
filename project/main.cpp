﻿#include <set>
#include "testdata.h"
#include "sdf_naive.h"
#include "sdf_accelerated.h"
#include "get_error.h"
#include "visualisation.h"

using namespace Eigen;
using DistanceTree = ComputeDistances::DistanceTree;

int main(int argc, char *argv[]) {
    enum experiment_type {naive, accelerated};
    std::vector<experiment_type> experiment;
    std::set<experiment_type> plots;
    std::vector<TestData::input_object> input_objects;

    // general input -------------------------------------------------------------------------------------------------------------------------------

    /* Not all objects work in all sizes. The explanation for this can be found in the written elaboration.
     * Well work the meshes
     *      bunny_1k
     *      bunny_1mio
     *      bunny_4mio
     *      armadillo
     *      buddha
     * with a resize_factor of 1 and a cube_length of 256.
     */

    input_objects = {TestData::input_object::bunny_1k}; // see options in testdata.h
    float resize_factor_object = 1; // size of the object within the distance field
    int cube_length = 256; // size of one dimension in the distance field
    experiment = {accelerated, naive}; // experiment type can be naive and / or accelerated

    // input for accelerated experiment
    float tree_cutoff = 0.75; // 0: use bounding box of the entire object, 1: use bounding box of each triangle
    SDFAccelerated::fmm_method method = SDFAccelerated::fmm_method::normal; // see options in accelerated.h
                                                                            // high_accuracy and distance_solver are not thoroughly tested!

    // visualization input
    plots = {accelerated};
    bool show_original_mesh = true; // can crash with dragon_7mio

    // error input
    bool measure_error = true;
    // ---------------------------------------------------------------------------------------------------------------------------------------------

    for (auto& it: input_objects) {

        MatrixXd dimensions_distance_field(2, 3);
        dimensions_distance_field << cube_length - 1, cube_length - 1, cube_length - 1, 0, 0, 0;

        // get object
        std::unique_ptr<MatrixXd> V = std::unique_ptr<MatrixXd>(new MatrixXd());
        std::unique_ptr<MatrixXi> F = std::unique_ptr<MatrixXi>(new MatrixXi());
        std::unique_ptr<MatrixXd> dimensions_object = std::unique_ptr<MatrixXd>(new MatrixXd(2, 3));
        TestData::get_object(resize_factor_object, it, cube_length, V.get(), F.get(), dimensions_object.get());

        // get bounding box hierarchy
        std::chrono::steady_clock::time_point begin_dt = std::chrono::steady_clock::now();
        std::unique_ptr<DistanceTree> distance_tree = std::unique_ptr<DistanceTree>(new DistanceTree());
        ComputeDistances::get_distance_tree(V.get(), F.get(), distance_tree.get());
        std::chrono::steady_clock::time_point end_dt = std::chrono::steady_clock::now();
        int timing_dt = std::chrono::duration_cast<std::chrono::milliseconds>(end_dt - begin_dt).count();

        // output initialization
        VectorXd bb_max = distance_tree->aabb_tree.m_box.max();
        VectorXd bb_min = distance_tree->aabb_tree.m_box.min();
        VectorXd diff = bb_max - bb_min;
        float object_size = diff(0) * diff(1) * diff(2);
        std::cout << std::endl;
        std::cout << "number voxels distance field: " << cube_length << "^3" << std::endl;
        std::cout << "number triangles object: " << F->rows() << std::endl;
        std::cout << "number points object: " << V->rows() << std::endl;
        std::cout << "outer bounding box takes up " << (object_size / pow(cube_length, 3)) * 100 << "% of the distance field" << std::endl;
        std::cout << "create bounding box hierarchy: " << timing_dt << "ms" << std::endl;

        // start experiments
        std::unique_ptr<VectorXd> sdf_naive = std::unique_ptr<VectorXd>(new VectorXd());
        std::unique_ptr<VectorXd> sdf_accelerated = std::unique_ptr<VectorXd>(new VectorXd());
        for (auto& it: experiment) {
            if (it == naive) {
                SDFNaive::sdf_naive(distance_tree.get(), dimensions_distance_field, sdf_naive.get());
            }
            if (it == accelerated) {
                SDFAccelerated::sdf_accelerated(distance_tree.get(), dimensions_distance_field, tree_cutoff, method, sdf_accelerated.get());
            }
        }

        // get error
        if (measure_error) {
            std::unique_ptr<double> mean_error = std::unique_ptr<double>(new double());
            std::unique_ptr<double> max_error = std::unique_ptr<double>(new double());
            GetError::get_error(sdf_naive.get(), sdf_accelerated.get(), mean_error.get(), max_error.get());

            std::cout << "mean error: " << *mean_error << std::endl;
            std::cout << "max error: " << *max_error << std::endl;
        }

        // plot
        // 3: see original mesh
        // 0: sdf at isolevel 0
        // 1: isolevel++
        // 2: isolevel--
        for (auto& it: plots) {
            if (it == naive) {
                Visualization::plot(V.get(), F.get(), sdf_naive.get(), dimensions_distance_field, show_original_mesh);
            }
            if (it == accelerated) {
                Visualization::plot(V.get(), F.get(), sdf_accelerated.get(), dimensions_distance_field, show_original_mesh);
            }
        }

    }

    return 0;
}
