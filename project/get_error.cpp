#include "get_error.h"
#include "Eigen/Eigen"

namespace GetError {
    using namespace Eigen;

void get_error(VectorXd* sdf_naive, VectorXd* sdf_accelerated, double* mean_error, double* max_error) {

    double accumulated_error = 0;
    double inner_max = 0;
    for (int i = 0; i < sdf_naive->size(); i++) {
        double error = abs((*sdf_naive)(i) - (*sdf_accelerated)(i));
        accumulated_error += error;
        inner_max = std::max(inner_max, error);
    }
    *mean_error = accumulated_error / sdf_naive->size();
    *max_error = inner_max;
}
}
