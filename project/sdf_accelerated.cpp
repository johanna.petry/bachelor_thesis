#include "sdf_accelerated.h"
#include <iostream>
#include <set>
#include <chrono>
#include "fast_marching_method.hpp"

namespace SDFAccelerated {

using namespace Eigen;
namespace fmm = thinks::fast_marching_method;
using DistanceTree = ComputeDistances::DistanceTree;

typedef igl::AABB<Eigen::MatrixXd, 3> Tree;
typedef std::array<int, 3> voxel;

struct VoxelHasher {
    std::size_t operator()(const voxel& a) const {
        std::size_t h = 0;

        for (auto e : a) {
            h ^= std::hash<int>{}(e)  + 0x9e3779b9 + (h << 6) + (h >> 2);
        }
        return h;
    }
};

typedef std::pair<double, voxel> elem;

class QueueComparator {
  public:
    bool operator() (const elem &l, const elem &r) {
        if (abs(l.first) <= abs(r.first)) {
            return false;
        }
        return true;
    }
};

typedef std::unordered_map<voxel, double, VoxelHasher> HashMap;
typedef std::priority_queue<elem, std::vector<elem>, QueueComparator> Queue;

class BreakCondition {
    private:
        double inner_diff;
        double outer_diff;
        double current_inner_distance;
        double current_outer_distance;
        double last_recomputed_inner_distance;
        double last_recomputed_outer_distance;

   public:
        BreakCondition(double current_inner_distance,
                        double current_outer_distance,
                        double last_recomputed_inner_distance,
                        double last_recomputed_outer_distance) {
            this->current_inner_distance = current_inner_distance;
            this->current_outer_distance = current_outer_distance;
            this->last_recomputed_inner_distance = last_recomputed_inner_distance;
            this->last_recomputed_outer_distance = last_recomputed_outer_distance;
            this->inner_diff = current_inner_distance - last_recomputed_inner_distance;
            this->outer_diff = current_outer_distance - last_recomputed_outer_distance;
        }

        void update_break_condition(bool positive, std::set<voxel>* recomputed_voxel, voxel element, double distance) {
            if (recomputed_voxel->find(element) != recomputed_voxel->end()) {
                if (positive) {
                    this->last_recomputed_outer_distance = std::max(distance, this->last_recomputed_outer_distance);
                }
                else {
                    this->last_recomputed_inner_distance = std::min(distance, this->last_recomputed_inner_distance);
                }
                recomputed_voxel->erase(element);
            }
            if (positive) {
                this->current_outer_distance = distance;
                if (this->last_recomputed_outer_distance == 0) {
                    this->last_recomputed_outer_distance = this->current_outer_distance;
                }
            }
            else {
                this->current_inner_distance = distance;
                if (this->last_recomputed_inner_distance == 0) {
                    this->last_recomputed_inner_distance = this->current_inner_distance;
                }
            }

            this->outer_diff = this->current_outer_distance - this->last_recomputed_outer_distance;
            this->inner_diff = this->current_inner_distance - this->last_recomputed_inner_distance;
        }

        bool continue_loop() {
            return ((this->outer_diff <= 1) && (this->inner_diff >= -1));
        }
};

void add_voxel(Tree* tree, std::set<voxel>* set_voxel) {
    Vector3d vec_buffer(1, 1, 1);

    VectorXd max_val = tree->m_box.max();
    VectorXd min_val = tree->m_box.min();

    max_val += vec_buffer;
    min_val -= vec_buffer;

    // round to integer
    for (int i = 0; i < 3; i++) {
        max_val(i) = ceil(max_val(i));
        min_val(i) = floor(min_val(i));
    }

    // add grid points to set
    for (int x = min_val(0); x <= max_val(0); x++) {
        for (int y = min_val(1); y <= max_val(1); y++) {
            for (int z = min_val(2); z <= max_val(2); z++) {
                set_voxel->insert({{x, y, z}});

            }
        }
    }
}

void rec(Tree* tree, std::set<voxel>* set_voxel, int depth) {
    if (tree->is_leaf()) {
        add_voxel(tree, set_voxel);
        return;
    }
    if (depth <= 0) {
        add_voxel(tree, set_voxel);
        return;
    }
    else {
        rec(tree->m_left, set_voxel, depth - 1);
        rec(tree->m_right, set_voxel, depth - 1);
    }
}

void get_boundary(DistanceTree* distance_tree, int depth, std::set<voxel>* set_voxel) {
    Tree* tree = &distance_tree->aabb_tree;
    rec(tree, set_voxel, depth);
}

VectorXd binary_distance_field(HashMap* hash_map, MatrixXd dimensions) {
    // just for testing

    VectorXd size_vec = dimensions.row(0) - dimensions.row(1) + RowVector3d(1, 1, 1);
    int size = size_vec(0) * size_vec(1) * size_vec(2);

    MatrixXd indices(hash_map->size(), 3);
    int counter = 0;
    for (auto& it: (*hash_map)) {
        indices.row(counter) = RowVector3d(it.first[0], it.first[1], it.first[2]);
        counter++;
    }

    VectorXd field(size);
    for (int i = 0; i < size; i++) {
        field(i) = 1;
    }
    for (int i = 0; i < indices.rows(); i++) {
        int index = indices(i, 2) * (size_vec(0) * size_vec(1)) + indices(i, 1) * size_vec(0) + indices(i, 0);
        field(index) = 0;
    }

    return field;
}

VectorXd binary_distance_field(MatrixXd* indices, MatrixXd dimensions) {
    // just for testing

    VectorXd size_vec = dimensions.row(0) - dimensions.row(1) + RowVector3d(1, 1, 1);
    int size = size_vec(0) * size_vec(1) * size_vec(2);

    VectorXd field(size);
    for (int i = 0; i < size; i++) {
        field(i) = 1;
    }
    for (int i = 0; i < indices->rows(); i++) {
        int index = (*indices)(i, 2) * (size_vec(0) * size_vec(1)) + (*indices)(i, 1) * size_vec(0) + (*indices)(i, 0);
        field(index) = 0;
    }

    return field;
}


std::tuple<bool, double> fmm_approx(voxel element, HashMap* hash_map) {
    std::vector<double> distances;
    int sign = 1;

    for (int dim = 0; dim < 3; dim++) {
        voxel neighbour = {{element[0], element[1], element[2]}};
        double min_distance = std::numeric_limits<double>::infinity();

        int dir = -1;
        neighbour[dim] += dir;

        if (hash_map->find(neighbour) != hash_map->end()) {
            min_distance = abs((*hash_map)[neighbour]);
            if ((*hash_map)[neighbour] < 0) {
                sign = -1;
            }
        }

        dir = 2; // -1 + 2 = 1
        neighbour[dim] += dir;

        if (hash_map->find(neighbour) != hash_map->end()) {
            min_distance = std::min(min_distance, abs((*hash_map)[neighbour]));
            if ((*hash_map)[neighbour] < 0) {
                sign = -1;
            }
        }

        if (min_distance < std::numeric_limits<double>::infinity()) {
            distances.push_back(min_distance);
        }
    }

    double approx;

    if (distances.size() == 1) {
        approx = distances[0] + 1;
    }
    else {
        double a = distances.size();
        double b = 0;
        double c = -1;
        for (int i = 0; i < distances.size(); i++) {
            b += -2 * distances[i];
            c += distances[i] * distances[i];
        }
        approx = (-b + sqrt(b * b - 4 * a * c)) / (2 * a);
    }

    bool valid = true;
    if (isnan(approx)) {
        valid = false;
        return std::make_tuple(valid, sign * approx);
    }
    for (int i = 0; i < distances.size(); i++) {
        if (distances[i] > approx) {
            valid = false;
            return std::make_tuple(valid, sign * approx);
        }
    }

    return std::make_tuple(valid, sign * approx);
}

void get_distance(voxel element, HashMap* hash_map, Queue* queue, DistanceTree* distance_tree,
                 std::set<voxel>* recomputed_voxels) {

    std::tuple<bool, double> fmm_approx_result = fmm_approx(element, hash_map);
    double distance = std::get<1>(fmm_approx_result);
    bool valid = std::get<0>(fmm_approx_result);

    if (!valid) { // recompute
        MatrixXd index_eigen(1, 3);
        index_eigen << element[0], element[1], element[2];
        std::unique_ptr<VectorXd> distance_eigen = std::unique_ptr<VectorXd>(new VectorXd());
        ComputeDistances::distance_to_object(&index_eigen, distance_tree, distance_eigen.get());
        distance = (*distance_eigen)(0);

        recomputed_voxels->insert(element);
    }

    queue->push(make_pair(distance, element));
}

void compute_neighbourhood(voxel element, HashMap* hash_map, Queue* queue, ComputeDistances::DistanceTree* distance_tree,
                          std::set<voxel>* recomputed_voxel) {

    for (int dim = 0; dim < 3; dim++) {
        for (int dir = -1; dir < 2; dir += 2) {

            voxel neighbour = {{element[0],element[1], element[2]}};
            neighbour[dim] += dir;

            // just compute if neighbour is not already frozen
            if (hash_map->find(neighbour) == hash_map->end()) {
                get_distance(neighbour, hash_map, queue, distance_tree, recomputed_voxel);
            }
        }
    }
}

void correct_boundary(MatrixXd* voxels, VectorXd* distances, ComputeDistances::DistanceTree* distance_tree, HashMap* hash_map) {

    std::unique_ptr<Queue> queue = std::unique_ptr<Queue>(new Queue());
    std::unique_ptr<std::set<voxel>> recomputed_voxel = std::unique_ptr<std::set<voxel>>(new std::set<voxel>());

    // delete all voxels that are far away from the object
    std::vector<int> keep;
    std::vector<int> keepP;
    std::vector<int> keepM;
    for (int i = 0; i < distances->size(); i++) {
        if (abs((*distances)(i)) <= 1.5) {
            keep.push_back(i);
            if((*distances)(i)<=0){
                keepM.push_back(i);
            }
            else{
                keepP.push_back(i);
            }
        }
    }

    // initialize frozen voxels
    for (int i = 0; i < keep.size(); i++) {
        int index = keep[i];
         voxel element = {{(int) ((*voxels)(index, 0)+0.5), (int) ((*voxels)(index, 1)+0.5), (int) ((*voxels)(index, 2)+0.5)}};
         double distance = (*distances)(keep[i]);
         (*hash_map)[element] = distance;
    }

    // initialize narrow band indices
    std::set<voxel> narrow_band_indices;
    for (auto& it: *hash_map) {
        voxel element = it.first;
        for (int dim = 0; dim < 3; dim++) {
            for (int dir = -1; dir < 2; dir += 2) {
                voxel neighbour = {{element[0], element[1], element[2]}};
                neighbour[dim] += dir;
                if (hash_map->find(neighbour) == hash_map->end()) {
                    narrow_band_indices.insert(neighbour);
                }
            }
        }
    }

    // initialize narrow band distances
    for (auto& it: narrow_band_indices) {
        get_distance(it, hash_map, queue.get(), distance_tree, recomputed_voxel.get());
    }

    // start fmm
    BreakCondition break_condition(0, 0, 0, 0);
    while (break_condition.continue_loop()) { // break if we have one layer of voxels with approximated distances
        voxel element = queue->top().second;
        double distance = queue->top().first;
        queue->pop();

        // we put voxels mulitiple times in queue and just use the first because it has the smallest distance
        if (hash_map->find(element) != hash_map->end()) {
            continue;
        }

        // freeze voxel
        (*hash_map)[element] = distance;
        bool positive = true;
        if (distance < 0) {
            positive = false;
        }

        // update break condition
        break_condition.update_break_condition(positive, recomputed_voxel.get(), element, distance);

        // update narrow band
        compute_neighbourhood(element, hash_map, queue.get(), distance_tree, recomputed_voxel.get());
    }
}

void fast_marching_method(HashMap* hash_map, MatrixXd dimensions, fmm_method method, VectorXd* distance_field) {
    // uses https://github.com/thinks/fast-marching-method

    VectorXd distance_field_size = dimensions.row(0) - dimensions.row(1) + RowVector3d(1, 1, 1);

    // convert voxel and distances
    std::vector<voxel> set_voxel(hash_map->size());
    std::vector<double> distances(hash_map->size());
    int counter = 0;
    for (auto& it: (*hash_map)) {
        set_voxel[counter] = it.first;
        distances[counter] = it.second;

        counter++;
    }

    // fmm params
    auto grid_size = std::array<size_t, 3>{{(long unsigned int) distance_field_size(0), (long unsigned int) distance_field_size(1), (long unsigned int) distance_field_size(2)}};
    auto grid_spacing = std::array<double, 3>{{1, 1, 1}};
    auto uniform_speed = 1;

    // fmm
    std::vector<double> arrival_times;
    switch(method) {
        case fmm_method::normal:
            arrival_times = fmm::SignedArrivalTime(grid_size, set_voxel, distances,
                                                   fmm::UniformSpeedEikonalSolver<double, 3>(grid_spacing, uniform_speed));
            break;
        case fmm_method::high_accuracy:
            arrival_times = fmm::SignedArrivalTime(grid_size, set_voxel, distances,
                                               fmm::HighAccuracyUniformSpeedEikonalSolver<double, 3>(grid_spacing, uniform_speed));
            break;
        case fmm_method::distance_solver:
         arrival_times = fmm::SignedArrivalTime(grid_size, set_voxel, distances,
                                               fmm::DistanceSolver<double, 3>(uniform_speed));
            break;
        default:
            arrival_times = fmm::SignedArrivalTime(grid_size, set_voxel, distances,
                                               fmm::UniformSpeedEikonalSolver<double, 3>(grid_spacing, uniform_speed));
    }

    // convert to VectorXd
    VectorXd distance_field_result(arrival_times.size());
    for (int i = 0; i < arrival_times.size(); i++) {
        distance_field_result(i) = arrival_times[i];
    }

    (*distance_field) = distance_field_result;
}


void sdf_accelerated(DistanceTree* distance_tree, MatrixXd dimensions, float tree_cutoff, fmm_method method, VectorXd* distance_field) {

    std::chrono::steady_clock::time_point begin = std::chrono::steady_clock::now();

    // get bounding boxes from DistanceTree add buffer and get voxel
    int depth = log2(distance_tree->aabb_tree.subtree_size()) * tree_cutoff;
    std::unique_ptr<std::set<voxel>> set_boundary_voxel = std::unique_ptr<std::set<voxel>>(new std::set<voxel>());
    get_boundary(distance_tree, depth, set_boundary_voxel.get());

    // convert so we can use igl to compute distances
    std::unique_ptr<MatrixXd> vec_boundary_voxel = std::unique_ptr<MatrixXd>( new MatrixXd(set_boundary_voxel->size(), 3));
    int counter = 0;
    for (std::set<voxel>::iterator it = set_boundary_voxel->begin(); it != set_boundary_voxel->end(); ++it) {
        vec_boundary_voxel->row(counter) = RowVector3d((*it)[0], (*it)[1], (*it)[2]);
        counter++;
    }

    //(*distance_field) = binary_distance_field(vec_boundary_voxel.get(), dimensions);

    // get distances
    std::unique_ptr<VectorXd> boundary_distances = std::unique_ptr<VectorXd>(new VectorXd());
    ComputeDistances::distance_to_object(vec_boundary_voxel.get(), distance_tree, boundary_distances.get());

    // delete all voxel that are far away from the object and start modified fmm method
    std::unique_ptr<HashMap> hash_map = std::unique_ptr<HashMap>(new HashMap());
    correct_boundary(vec_boundary_voxel.get(), boundary_distances.get(), distance_tree, hash_map.get());

    //(*distance_field) = binary_distance_field(hash_map.get(), dimensions);

    // fast marching method for all other voxel
    fast_marching_method(hash_map.get(), dimensions, method, distance_field);

    std::chrono::steady_clock::time_point end = std::chrono::steady_clock::now();
    int timing = std::chrono::duration_cast<std::chrono::milliseconds>(end - begin).count();

    // output
    std::cout << "\n" << "distance field with fast marching method:" << std::endl;
    std::cout << "tree cutoff at " << tree_cutoff << std::endl;
    std::cout << timing << "ms" << std::endl;
}
}
