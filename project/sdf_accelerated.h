#ifndef DF_TIGHT_BOUNDARY_H
#define DF_TIGHT_BOUNDARY_H

#include "compute_distances.h"

namespace SDFAccelerated {

using namespace Eigen;
enum class fmm_method {normal, high_accuracy, distance_solver};

void sdf_accelerated(ComputeDistances::DistanceTree* distance_tree, MatrixXd dimensions, float tree_cutoff, fmm_method method, VectorXd* distance_field);
}

#endif // DF_TIGHT_BOUNDARY_H
